# Saisir la réponse

Parce que Anki peut aussi être très beau !

Modèle de carte où on peut saisir la réponse avec un affichage différencié du verso selon l'exactitude de la réponse aux couleurs de [wanikani](https://www.wanikani.com).

Vous pouvez ouvrir le fichier [exemple_sens_unique](/exemple_sens_unique.apkg) ou [exemple_double_sens](/exemple_double_sens.apkg) avec Anki pour ajouter le type de note `WK-Style Sens Unique avec saisie` ou `WK-Style Double Sens avec saisie` à Anki et créer vos propres cartes avec ce type de note.

## Captures d'écran

### Desktop

#### Réponse incorrecte

|Recto|Verso|
|:---:|:---:|
|![](/images/desktop-incorrect-recto.png)|![](/images/desktop-incorrect-verso.png)

#### Réponse correcte

|Recto|Verso|
|:---:|:---:|
|![](/images/desktop-correct-recto.png)|![](/images/desktop-correct-verso.png)

### AnkiMobile

#### Réponse incorrecte

|Recto|Verso|
|:---:|:---:|
|![](/images/ankimobile-incorrect-recto.png)|![](/images/ankimobile-incorrect-verso.png)

#### Réponse correcte

|Recto|Verso|
|:---:|:---:|
|![](/images/ankimobile-correct-recto.png)|![](/images/ankimobile-correct-verso.png)
